#!/bin/bash

export TOP_SRCDIR="../source"
PB_DIR="bindings/python"

die () {
    echo "$1" >&2
    exit 1
}

# rename lasso.py to be sure that we test the installed one
find $TOP_SRCDIR/$PB_DIR -maxdepth 1 -name "*.py" -exec mv {} {}.backup \;

# run the tests
TESTS="tests/binding_tests.py tests/profiles_tests.py"

for t in $TESTS; do
    echo -n "Test $t ... "
    python3 "${TOP_SRCDIR}/${PB_DIR}/${t}" >"${TOP_SRCDIR}/${PB_DIR}/${t}.log" 2>&1
    RET=$?
    if [ $RET = 0 ]; then
        echo "ok"
    else
        echo "failed"
        cat "${TOP_SRCDIR}/${PB_DIR}/${t}.log"
        die "Test ${t} failed"
    fi
done

exit 0
